# sync repos

It is used to sync the repository 1 with repository 2

This script run as

First argument should be either -s or -h

1
-s when you have public key in both repo (It is essential to set up a ssh connection  on both repo for smooth performance ).
if you haven’t follow 
https://build-me-the-docs-please.readthedocs.io/en/latest/Using_Git/SetUpSSHForGit.html  
This will not prompt you for password in private repositories.

2
-h when you have http in both repo (It prompt you for a password whenever required.)
if you haven’t follow 

Second Argument is alias of server1 from which you are fetching data.
Third Argument is alias of server2 to which you want to sync with.

eg.
ssh2	git@github.com:PrashantKumar1291998/test12.git (fetch)

ssh2	git@github.com:PrashantKumar1291998/test12.git (push)

ssh3	git@gitlab.com:prashant.kumar1/test1.git (fetch)

ssh3	git@gitlab.com:prashant.kumar1/test1.git (push)

ssh2 repo from which we are fetching data to ssh3 repo and sync with.

Use,

script  -s ssh2 ssh3

and then your ssh2 repo data will be copied to ssh3 repo branch wise.

Like – Master data of ssh2 will be sync with master of ssh3
	new branch  data of ssh2 will be in sync with new  of ssh3 (if branch is not existed earlier on 	ssh3 it will create)



